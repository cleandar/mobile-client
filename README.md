# Cleandar Mobile Client

## Development

You need to have [the server](https://gitlab.com/cleandar/server) up and running
in local, to be able to work with the client locally.

```bash
yarn install
yarn start
```
