import { useEffect, useRef, useState } from 'react'

export default () => {
  const [token, setToken] = useState('')
  const [loggedIn, setLoggedIn] = useState(false)
  const [expiresAt, setExpiresAt] = useState<Date>()

  const timeout = useRef<number>()

  const handleTokenRefresh = async () => {
    const storedToken = await localStorage.getItem('token')

    if (storedToken) {
      try {
        const data = await fetch(
          `${process.env.REACT_APP_API_URL}/token-refresh/`,
          {
            method: 'POST',
            body: JSON.stringify({ token: storedToken }),
          }
        ).then(response => response.json())

        const loggedIn = !data.errors

        if (loggedIn) {
          setToken(storedToken)

          const date = new Date()
          date.setSeconds(date.getSeconds() + data.expires_in)
          setExpiresAt(date)
        }

        setLoggedIn(loggedIn)

        return
      } catch (e) {
        setLoggedIn(false)
        localStorage.setItem('token', '')
      }
    }
  }

  useEffect(() => {
    handleTokenRefresh().then()
  }, [])

  useEffect(() => {
    if (!token || !expiresAt) {
      timeout.current = undefined
      return
    }

    timeout.current = window.setTimeout(() => {
      const now = new Date()
      const diff = (expiresAt.getTime() - now.getTime()) / 1000
      if (diff < 120000) {
        handleTokenRefresh().then(() => console.log('Token refreshed'))
      }
    }, 60000)

    return () => {
      clearTimeout(timeout.current)
    }
  }, [token, expiresAt])

  const logIn = async (email: string, password: string): Promise<void> => {
    const data = await fetch(`${process.env.REACT_APP_API_URL}/token-auth/`, {
      method: 'POST',
      body: JSON.stringify({
        email,
        password,
      }),
    }).then(response => response.json())

    const loggedIn = !data.errors

    if (loggedIn) {
      setToken(data.token)

      const date = new Date()
      date.setSeconds(date.getSeconds() + data.expires_in)
      setExpiresAt(date)

      localStorage.setItem('token', data.token)
    }

    setLoggedIn(loggedIn)

    if (!loggedIn) {
      throw Error(`Could not log in: ${JSON.stringify(data.errors)}`)
    }
  }

  return { loggedIn, token, logIn }
}
