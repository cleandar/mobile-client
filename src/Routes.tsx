import React from 'react'
import { IonReactRouter } from '@ionic/react-router'
import {
  IonApp,
  IonIcon,
  IonLabel,
  IonRouterOutlet,
  IonTabBar,
  IonTabButton,
  IonTabs,
} from '@ionic/react'
import { Redirect, Route } from 'react-router-dom'
import Login from './pages/Login'
import {
  ApolloClient,
  ApolloProvider,
  createHttpLink,
  InMemoryCache,
} from '@apollo/client'
import Tasks from './pages/Tasks'
import TaskDetail from './pages/TaskDetail'
import EditTask from './pages/EditTask'
import Calendar from './pages/Calendar'
import Preferences from './pages/Preferences'
import { calendar, list, options } from 'ionicons/icons'

interface Props {
  loggedIn: boolean
  token: string
  logIn: (email: string, password: string) => Promise<void>
}

const Routes = (props: Props) => {
  if (!props.loggedIn) {
    return (
      <IonReactRouter basename={process.env.REACT_APP_BASE_URL}>
        <IonRouterOutlet>
          <Route path="/login" exact={true}>
            <Login logIn={props.logIn} />
          </Route>
          <Redirect to="/login" />
        </IonRouterOutlet>
      </IonReactRouter>
    )
  }

  const client = new ApolloClient({
    cache: new InMemoryCache(),
    link: createHttpLink({
      uri: process.env.REACT_APP_API_URL + '/graphql',
      credentials: 'same-origin',
      headers: {
        Authorization: props.token ? `Bearer ${props.token}` : '',
      },
    }),
  })

  return (
    <ApolloProvider client={client}>
      <IonApp>
        <IonReactRouter basename={process.env.REACT_APP_BASE_URL}>
          <IonTabs>
            <IonRouterOutlet>
              <Route path="/tasks" component={Tasks} exact={true} />
              <Route path="/tasks/:id" component={TaskDetail} />
              <Route path="/tasks/:id/edit" component={EditTask} />
              <Route path="/calendar" component={Calendar} exact={true} />
              <Route path="/options" component={Preferences} />
              <Route path="/" render={() => <Redirect to="/tasks" />} />
            </IonRouterOutlet>
            <IonTabBar slot="bottom">
              <IonTabButton tab="tasks" href="/tasks">
                <IonIcon icon={list} />
                <IonLabel>Tasques</IonLabel>
              </IonTabButton>
              <IonTabButton tab="calendar" href="/calendar">
                <IonIcon icon={calendar} />
                <IonLabel>Calendari</IonLabel>
              </IonTabButton>
              <IonTabButton tab="options" href="/options">
                <IonIcon icon={options} />
                <IonLabel>Opcions</IonLabel>
              </IonTabButton>
            </IonTabBar>
          </IonTabs>
        </IonReactRouter>
      </IonApp>
    </ApolloProvider>
  )
}

export default React.memo(
  Routes,
  (prevProps: Props, nextProps: Props) =>
    prevProps.loggedIn === nextProps.loggedIn &&
    prevProps.token === nextProps.token
)
