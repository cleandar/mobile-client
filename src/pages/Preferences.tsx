import React from 'react'
import {
  IonButton,
  IonContent,
  IonHeader,
  IonItem,
  IonPage,
  IonTitle,
  IonToolbar,
} from '@ionic/react'

const Preferences = () => {
  const handleLogOut = () => {
    localStorage.setItem('token', '') // TODO handle in useAuth
    window.location.reload()
  }

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Preferences</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonItem>
          <IonButton onClick={handleLogOut}>Tancar sessió</IonButton>
        </IonItem>
      </IonContent>
    </IonPage>
  )
}

export default Preferences
