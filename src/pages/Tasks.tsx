import {
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonCardTitle,
  IonContent,
  IonFab,
  IonFabButton,
  IonFabList,
  IonHeader,
  IonIcon,
  IonItem,
  IonLabel,
  IonList,
  IonPage,
  IonRefresher,
  IonRefresherContent,
  IonSkeletonText,
  IonTitle,
  IonToolbar,
  useIonViewDidEnter,
  useIonViewDidLeave,
} from '@ionic/react'
import React, { useEffect, useRef, useState } from 'react'
import { add, folderOpen } from 'ionicons/icons'
import { Room, ScheduleType, Task } from '../domain'
import './Tasks.css'
import TaskForm, { TaskData } from '../components/TaskForm'
import RoomForm, { RoomData } from '../components/RoomForm'
import { RefresherEventDetail } from '@ionic/core'
import { PluginListenerHandle, Plugins } from '@capacitor/core'
import { gql, useMutation, useQuery } from '@apollo/client'

const ALL_ROOMS = gql`
  query {
    rooms {
      id
      name
      family {
        id
        name
      }
      taskSet {
        id
        name
        scheduleType
        instanceSeparation
        daysOfWeek
        dueDate
        createdAt
        updatedAt
      }
    }
  }
`

const CREATE_TASK = gql`
  mutation createTask(
    $name: String!
    $roomId: ID!
    $scheduleType: ScheduleType!
    $instanceSeparation: Int
    $daysOfWeek: [Int]
    $dueDate: String
  ) {
    createTask(
      name: $name
      roomId: $roomId
      scheduleType: $scheduleType
      instanceSeparation: $instanceSeparation
      daysOfWeek: $daysOfWeek
      dueDate: $dueDate
    ) {
      task {
        name
        scheduleType
        instanceSeparation
        daysOfWeek
        dueDate
        # TODO uncomment when available
        #        completionSet {
        #          user {
        #            id
        #            firstName
        #            lastName
        #          }
        #          completedAt
        #        }
      }
    }
  }
`

const CREATE_ROOM = gql`
  mutation createRoom($name: String!, $familyId: ID!) {
    createRoom(name: $name, familyId: $familyId) {
      room {
        id
        name
      }
    }
  }
`

const Tasks = () => {
  const { loading, data, refetch } = useQuery<{ rooms: Room[] }>(ALL_ROOMS)
  const [addTask] = useMutation<
    { addTask: Task },
    {
      name: string
      roomId: number
      scheduleType: ScheduleType
      instanceSeparation: number
    }
  >(CREATE_TASK)
  const [addRoom] = useMutation<
    { room: Room },
    { name: string; familyId: number }
  >(CREATE_ROOM)

  const [showAddTaskModal, setShowAddTaskModal] = useState(false)
  const [showAddRoomModal, setShowAddRoomModal] = useState(false)

  const doRefresh = async (event?: CustomEvent<RefresherEventDetail>) => {
    setTimeout(async () => {
      await refetch()
      if (event) {
        event.detail.complete()
      }
    }, 500)
  }

  const backButtonListenerHandle = useRef<PluginListenerHandle>()

  const addBackButtonListener = () => {
    backButtonListenerHandle.current = Plugins.App.addListener(
      'backButton',
      Plugins.App.exitApp
    )
  }

  const removeBackButtonListener = () => {
    if (backButtonListenerHandle.current) {
      backButtonListenerHandle.current.remove()
      backButtonListenerHandle.current = undefined
    }
  }

  useIonViewDidEnter(addBackButtonListener)
  useIonViewDidLeave(removeBackButtonListener)

  useEffect(() => {
    if (showAddTaskModal || showAddRoomModal) {
      removeBackButtonListener()
    } else {
      addBackButtonListener()
    }
  }, [showAddTaskModal, showAddRoomModal])

  const handleAddTask = async (task: TaskData) => {
    await addTask({
      variables: {
        name: task.name,
        roomId: task.roomId,
        scheduleType: task.scheduleType,
        instanceSeparation: task.instanceSeparation,
      },
    })
    await doRefresh()
    setShowAddTaskModal(false)
  }

  const handleAddRoom = async (room: RoomData) => {
    await addRoom({
      variables: {
        name: room.name,
        familyId: 1, // TODO
      },
    })
    await doRefresh()
    setShowAddRoomModal(false)
  }

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Tasques</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonRefresher slot="fixed" onIonRefresh={doRefresh}>
          <IonRefresherContent />
        </IonRefresher>

        <IonList>
          {loading
            ? Array(2)
                .fill(0)
                .map((_, i) => (
                  <IonCard key={i}>
                    <IonSkeletonText animated style={{ height: 130 }} />
                    <IonCardHeader>
                      <IonCardTitle>
                        <IonSkeletonText animated style={{ width: '60%' }} />
                      </IonCardTitle>
                    </IonCardHeader>
                    <IonCardContent>
                      {Array(2)
                        .fill(0)
                        .map((_, j) => (
                          <IonItem key={j}>
                            <IonSkeletonText
                              animated
                              style={{ width: '70%' }}
                            />
                          </IonItem>
                        ))}
                    </IonCardContent>
                  </IonCard>
                ))
            : data?.rooms.map(room => (
                <IonCard key={room.id}>
                  <img
                    src={process.env.REACT_APP_BASE_URL + '/assets/shapes.svg'}
                    alt=""
                    className="room-image"
                  />
                  <IonCardHeader>
                    <IonCardTitle>{room.name}</IonCardTitle>
                  </IonCardHeader>
                  <IonCardContent>
                    {room.taskSet.map(task => (
                      <IonItem key={task.id} routerLink={`/tasks/${task.id}`}>
                        <IonLabel>{task.name}</IonLabel>
                      </IonItem>
                    ))}
                  </IonCardContent>
                </IonCard>
              ))}
        </IonList>

        <IonFab vertical="bottom" horizontal="end" slot="fixed">
          <IonFabButton>
            <IonIcon icon={add} />
          </IonFabButton>

          <IonFabList side="top">
            <IonFabButton
              color="primary"
              onClick={() => setShowAddRoomModal(true)}
            >
              <IonIcon icon={folderOpen} />
            </IonFabButton>
            <IonFabButton
              color="primary"
              onClick={() => setShowAddTaskModal(true)}
            >
              <IonIcon icon={add} />
            </IonFabButton>
          </IonFabList>
        </IonFab>
      </IonContent>

      <TaskForm
        title="Afegir tasca"
        onSave={handleAddTask}
        isOpen={showAddTaskModal}
        onClose={() => setShowAddTaskModal(false)}
      />

      <RoomForm
        title="Afegir habitació"
        onSave={handleAddRoom}
        isOpen={showAddRoomModal}
        onClose={() => setShowAddRoomModal(false)}
      />
    </IonPage>
  )
}

export default Tasks
