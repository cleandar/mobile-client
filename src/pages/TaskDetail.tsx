import React from 'react'
import {
  IonBackButton,
  IonButtons,
  IonHeader,
  IonPage,
  IonToolbar,
  IonTitle,
  IonContent,
  IonSkeletonText,
} from '@ionic/react'
import { useParams } from 'react-router'
import { getNextDueDate, Task } from '../domain'
import { gql, useQuery } from '@apollo/client'

const getTask = gql`
  query task($id: ID!) {
    task(id: $id) {
      id
      name
      scheduleType
      instanceSeparation
      daysOfWeek
      dueDate
      # completionSet {
      #   id
      #   user {
      #     id
      #     firstName
      #     lastName
      #   }
      #   completedAt
      # }
      room {
        id
        name
      }
      createdAt
      updatedAt
    }
  }
`

type NextDueDateProps = {
  task: Task
}

const NextDueDate = (props: NextDueDateProps) => {
  const date = getNextDueDate(props.task)

  if (!date) {
    return null
  }

  let color = 'black'
  const today = new Date()
  if (date < today) {
    color = 'red'
  } else if (date === today) {
    color = 'orange'
  }

  const dateText = <span style={{ color }}>{date.toLocaleDateString()}</span>

  return <span>Pròxima data: {dateText}</span>
}

const TaskDetail = () => {
  const { id } = useParams<{ id: string }>()
  const taskId = parseInt(id)

  const { loading, data } = useQuery<{ task: Task }>(getTask, {
    variables: { id: taskId },
  })

  const task = data?.task

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonBackButton defaultHref="/tasks" />
          </IonButtons>
          <IonTitle>{task?.name}</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent className="ion-padding">
        {loading ? (
          <p>
            <IonSkeletonText
              animated
              style={{ width: '60%', lineHeight: '1em' }}
            />
          </p>
        ) : (
          task && (
            <>
              <p>Descripció: {task.name}</p>
              {task?.completionSet && (
                <p>
                  Últim dia completat:{' '}
                  {task?.completionSet[
                    task.completionSet.length
                  ].completedAt.toLocaleDateString()}
                </p>
              )}
              <p>
                <NextDueDate task={task} />
              </p>
            </>
          )
        )}
      </IonContent>
    </IonPage>
  )
}

export default TaskDetail
