import React, { useState } from 'react'
import {
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonContent,
  IonHeader,
  IonPage,
  IonTitle,
  IonToolbar,
} from '@ionic/react'
import ReactCalendar from 'react-calendar'
import './Calendar.css'

const Calendar = () => {
  const [date, setDate] = useState(new Date())

  const handleDateChange = (newDate: Date | Date[]) => {
    if (newDate instanceof Date) {
      setDate(newDate)
      return
    }
    setDate(newDate[0])
  }

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Calendari</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent>
        <ReactCalendar value={date} onChange={handleDateChange} />
        <IonCard>
          <IonCardHeader>
            <IonTitle>{date.toLocaleDateString()}</IonTitle>
          </IonCardHeader>
          <IonCardContent>
            <em>No hi ha tasques pendents!</em>
          </IonCardContent>
        </IonCard>
      </IonContent>
    </IonPage>
  )
}

export default Calendar
