import React from 'react'
import { Formik, FormikHelpers } from 'formik'
import * as yup from 'yup'
import {
  IonButton,
  IonContent,
  IonHeader,
  IonInput,
  IonItem,
  IonLabel,
  IonList,
  IonPage,
  IonTitle,
  IonToolbar,
} from '@ionic/react'

interface Props {
  logIn: (email: string, password: string) => Promise<void>
}

interface LoginData {
  email: string
  password: string
}

const Login = (props: Props) => {
  const handleSubmit = (
    values: LoginData,
    formikHelpers: FormikHelpers<LoginData>
  ) => {
    props.logIn(values.email, values.password).catch(() => {
      formikHelpers.setErrors({
        password: 'Usuari o contrasenya incorrectes',
      })
      formikHelpers.setSubmitting(false)
    })
  }

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Iniciar sessió</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <Formik<LoginData>
          initialValues={{ email: '', password: '' }}
          onSubmit={handleSubmit}
          validationSchema={yup.object<LoginData>().shape({
            email: yup
              .string()
              .email()
              .required(),
            password: yup
              .string()
              .min(6)
              .required(),
          })}
        >
          {({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
          }) => (
            <form onSubmit={handleSubmit}>
              <IonList lines="full" className="ion-no-margin ion-no-padding">
                <IonItem>
                  <IonLabel position="floating">Email</IonLabel>
                  <IonInput
                    type="text"
                    name="email"
                    onInput={handleChange}
                    onBlur={handleBlur}
                    value={values.email}
                  />
                  {errors.email && touched.email && errors.email}
                </IonItem>
                <IonItem>
                  <IonLabel position="floating">Contrasenya</IonLabel>
                  <IonInput
                    type="password"
                    name="password"
                    onInput={handleChange}
                    onBlur={handleBlur}
                    value={values.password}
                  />
                  {errors.password && touched.password && errors.password}
                </IonItem>

                <div className="ion-padding">
                  <IonButton
                    expand="block"
                    type="submit"
                    className="ion-no-margin"
                    disabled={isSubmitting}
                  >
                    Entrar
                  </IonButton>
                </div>
              </IonList>
            </form>
          )}
        </Formik>
      </IonContent>
    </IonPage>
  )
}

export default Login
