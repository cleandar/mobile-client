import { Task } from './task'

export type ScheduleType = 'REPEAT' | 'WEEKLY' | 'SINGLE'

function getNextRepeatDueDate(task: Task): Date {
  if (!task.completionSet?.length) {
    return new Date()
  }

  const nextDueDate =
    task.completionSet[task.completionSet.length - 1].completedAt

  nextDueDate.setDate(nextDueDate.getDate() + task.instanceSeparation)

  return nextDueDate
}

function getNextWeeklyDueDate(task: Task): Date {
  const yesterday = new Date()
  yesterday.setDate(yesterday.getDate() - 1)

  const lastCompletedDate =
    task.completionSet[task.completionSet.length - 1].completedAt
  const nextDueDate = lastCompletedDate || yesterday

  const lastDayNumber = nextDueDate.getDay()
  for (let i = 1; i <= 7; i++) {
    const dayNumber = ((lastDayNumber + i - 1) % 7) + 1
    if (task.daysOfWeek?.split(',').includes(dayNumber.toString())) {
      nextDueDate.setDate(nextDueDate.getDate() + i)
      break
    }
  }

  return nextDueDate
}

function getNextSingleDueDate(task: Task): Date | undefined {
  return task.dueDate
}

export function getNextDueDate(task: Task): Date | undefined {
  switch (task.scheduleType) {
    case 'REPEAT':
      return getNextRepeatDueDate(task)
    case 'WEEKLY':
      return getNextWeeklyDueDate(task)
    case 'SINGLE':
      return getNextSingleDueDate(task)
    default:
      return undefined
  }
}
