import { Family } from './family'

export interface User {
  firstName: string
  lastName: string
  email: string
  families: Family[]
}
