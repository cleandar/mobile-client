import { User } from './user'

export interface Completion {
  user: User
  completedAt: Date
}
