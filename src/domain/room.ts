import { Task } from './task'
import { Family } from './family'

export interface Room {
  id: number
  name: string
  family: Family
  taskSet: Task[]
}
