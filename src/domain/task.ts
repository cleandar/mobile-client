import { ScheduleType } from './schedule'
import { Room } from './room'
import { Completion } from './completion'

export interface Task {
  id: number
  name: string
  room: Room
  scheduleType: ScheduleType
  instanceSeparation: number
  daysOfWeek?: string
  dueDate?: Date
  completionSet: Completion[]
  createdAt: Date
  updatedAt: Date
}
