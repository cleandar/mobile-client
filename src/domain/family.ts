import { User } from './user'

export interface Family {
  name: string
  members: User[]
}
