import React, { useState } from 'react'
import { Room, Task } from '../domain'
import { Formik } from 'formik'
import {
  IonButton,
  IonButtons,
  IonContent,
  IonHeader,
  IonInput,
  IonItem,
  IonLabel,
  IonList,
  IonModal,
  IonTitle,
  IonToolbar,
} from '@ionic/react'
import * as yup from 'yup'

export type RoomData = Omit<Room, 'id' | 'family' | 'taskSet'>

interface Props {
  title?: string
  room?: RoomData
  onSave: (room: RoomData) => void
  isOpen: boolean
  onClose: () => void
}

const RoomForm = (props: Props) => {
  const [room] = useState<RoomData>(() => props.room || { name: '' })

  return (
    <IonModal isOpen={props.isOpen}>
      <IonHeader>
        <IonToolbar>
          <IonTitle>{props.title}</IonTitle>
          <IonButtons slot="end">
            <IonButton onClick={props.onClose}>Cancel·lar</IonButton>
          </IonButtons>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <Formik
          initialValues={room}
          onSubmit={props.onSave}
          validationSchema={yup.object<Task>().shape({
            name: yup.string().required(),
          })}
        >
          {({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
          }) => (
            <form onSubmit={handleSubmit}>
              <IonList lines="full" className="ion-no-margin ion-no-padding">
                <IonItem>
                  <IonLabel position="floating">Nom</IonLabel>
                  <IonInput
                    type="text"
                    name="name"
                    onInput={handleChange}
                    onBlur={handleBlur}
                    value={values.name}
                  />
                  {errors.name && touched.name && errors.name}
                </IonItem>

                <div className="ion-padding">
                  <IonButton
                    expand="block"
                    type="submit"
                    className="ion-no-margin"
                    disabled={isSubmitting}
                  >
                    Desar
                  </IonButton>
                </div>
              </IonList>
            </form>
          )}
        </Formik>
      </IonContent>
    </IonModal>
  )
}

export default RoomForm
