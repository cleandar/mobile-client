import React, { useEffect, useState } from 'react'
import { Room, Task } from '../domain'
import { Formik } from 'formik'
import {
  IonButton,
  IonButtons,
  IonContent,
  IonHeader,
  IonInput,
  IonItem,
  IonLabel,
  IonList,
  IonModal,
  IonSelect,
  IonSelectOption,
  IonTitle,
  IonToolbar,
} from '@ionic/react'
import * as yup from 'yup'
import { gql, useQuery } from '@apollo/client'

const rooms = gql`
  query {
    rooms {
      id
      name
    }
  }
`

export type TaskData = Omit<
  Task,
  'id' | 'room' | 'completionSet' | 'createdAt' | 'updatedAt'
> & { roomId: number }

const defaultTaskData: TaskData = {
  name: '',
  roomId: -1,
  scheduleType: 'REPEAT',
  instanceSeparation: 1,
}

interface Props {
  title?: string
  task?: TaskData
  onSave: (task: TaskData) => void
  isOpen: boolean
  onClose: () => void
}

const TaskForm = (props: Props) => {
  const [taskData, setTaskData] = useState<TaskData>(defaultTaskData)
  const { loading: loadingRooms, data: roomsData } = useQuery<{
    rooms: Room[]
  }>(rooms)

  useEffect(() => {
    setTaskData(props.task || defaultTaskData)
  }, [props.task])

  return (
    <IonModal isOpen={props.isOpen}>
      <IonHeader>
        <IonToolbar>
          <IonTitle>{props.title}</IonTitle>
          <IonButtons slot="end">
            <IonButton onClick={props.onClose}>Cancel·lar</IonButton>
          </IonButtons>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <Formik
          initialValues={taskData}
          onSubmit={props.onSave}
          validationSchema={yup.object<Task>().shape({
            name: yup.string().required(),
            roomId: yup
              .number()
              .moreThan(0)
              .required(),
            scheduleType: yup.string().required(),
          })}
        >
          {({
            values,
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            errors, // TODO
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            touched, // TODO
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
            isValid,
            dirty,
          }) => (
            <form onSubmit={handleSubmit}>
              <IonList lines="full" className="ion-no-margin ion-no-padding">
                <IonItem>
                  <IonLabel position="floating">Nom</IonLabel>
                  <IonInput
                    type="text"
                    name="name"
                    onInput={handleChange}
                    onBlur={handleBlur}
                    value={values.name}
                  />
                </IonItem>

                {!loadingRooms && (
                  <IonItem>
                    <IonLabel>Habitació</IonLabel>
                    <IonSelect
                      value={values.roomId}
                      defaultValue={-1}
                      onIonChange={handleChange}
                      name="roomId"
                    >
                      {roomsData?.rooms.map(room => (
                        <IonSelectOption key={room.id} value={room.id}>
                          {room.name}
                        </IonSelectOption>
                      ))}
                    </IonSelect>
                  </IonItem>
                )}

                <div className="ion-padding">
                  <IonButton
                    expand="block"
                    type="submit"
                    className="ion-no-margin"
                    disabled={isSubmitting || !isValid || !dirty}
                  >
                    Desar
                  </IonButton>
                </div>
              </IonList>
            </form>
          )}
        </Formik>
      </IonContent>
    </IonModal>
  )
}

export default TaskForm
