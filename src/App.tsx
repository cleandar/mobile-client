import React from 'react'
import '@ionic/react/css/core.css'
import '@ionic/react/css/normalize.css'
import '@ionic/react/css/structure.css'
import '@ionic/react/css/typography.css'
import '@ionic/react/css/padding.css'
import '@ionic/react/css/float-elements.css'
import '@ionic/react/css/text-alignment.css'
import '@ionic/react/css/text-transformation.css'
import '@ionic/react/css/flex-utils.css'
import '@ionic/react/css/display.css'
import './theme/variables.css'
import useAuth from './hooks/useAuth'
import Routes from './Routes'

const App = () => {
  const { loggedIn, token, logIn } = useAuth()

  return <Routes loggedIn={loggedIn} token={token} logIn={logIn} />
}

export default App
